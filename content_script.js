// Put all the javascript code here, that you want to execute after page load.

waitForElm('#attendance-widget').then((elm) => {

    waitForElm('#work-start').then((start) => {
        const form = elm.querySelector('form');
        let stempelButton = document.createElement("button");
        stempelButton.className = "stempelButton";
        stempelButton.textContent = start.value === "" ? "Einstempeln" : "Ausstempeln";
        stempelButton.addEventListener("click", stempeln);
        elm.insertBefore(stempelButton, form);
    });

});


function stempeln() {
    const now = Date.now();
    const date = new Date(now);
    const val = date.toLocaleTimeString('de-DE').substring(0, 5);

    waitForElm('#work-start').then((start) => {
        const btn = document.querySelector('.stempelButton');

        if(start.value === "") {
            start.value = val;
            start.focus();
        }
        else{
            const end = document.querySelector('#work-end');
            end.value = val;
            end.focus();
            btn.textContent = "Einstempeln";
            if((date.getHours() >= 12 && date.getMinutes() < 45) || date.getHours >= 13) {
                const breakstart = document.querySelector('#break-start');
                breakstart.value = "12:00";
                breakstart.focus();

                const breakend = document.querySelector('#break-end');
                breakend.value = "12:45";
                breakend.focus();
            }
        }
        btn.focus();
        start.closest('form').querySelector('button[type="submit"]').click();
    });
}

function waitForElm(selector) {
    return new Promise(resolve => {
        if (document.querySelector(selector)) {
            return resolve(document.querySelector(selector));
        }

        const observer = new MutationObserver(mutations => {
            if (document.querySelector(selector)) {
                resolve(document.querySelector(selector));
                observer.disconnect();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    });
}